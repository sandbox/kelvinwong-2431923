README.txt
==========

INTRODUCTION
------------
The Paragraphs view mode allows content editor to switch paragraph view modes in
node submit form.

REQUIREMENTS
------------
This module requires the following modules:
 * Paragraphs (https://drupal.org/project/paragraphs)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no configuration. When
moduel enabled, a view mode dropdown list will be added to paragraphs items
submission form when creating paragraph items.

MAINTAINERS
-----------
Current maintainers:
 * Kelvin Wong (KelvinWong) - https://drupal.org/user/62003
